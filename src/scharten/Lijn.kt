package scharten

enum class Lijn {
    O,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I;

    fun cwLijn() = if (this == O) I else Lijn.entries[this.ordinal - 1]
    fun counterLijn() = if (this == I) O else Lijn.entries[this.ordinal + 1]
}