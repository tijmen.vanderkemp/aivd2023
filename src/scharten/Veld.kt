package scharten

data class Veld(val lijn: Lijn, val ringNummer: RingNummer) {
    val notatie = lijn.toString().lowercase() + ringNummer.nummer

    override fun toString() = notatie

    fun aangrenzendVeld(richting: KardinaleRichting): Veld? = when (richting) {
        KardinaleRichting.NOORD -> when (ringNummer.nummer) {
            in 1..7 -> Veld(lijn, ringNummer + 1)
            // Verdwijnt van het bord af
            8 -> null
            else -> throw IllegalArgumentException()
        }

        KardinaleRichting.ZUID -> when (ringNummer.nummer) {
            in 2..8 -> Veld(lijn, ringNummer - 1)
            // Verdwijnt van het bord af
            1 -> null
            else -> throw IllegalArgumentException()
        }

        KardinaleRichting.CW -> if (lijn == Lijn.O) {
            // De notatie flipt hier, dus het ringnummer moet worden aangepast
            Veld(lijn.cwLijn(), RingNummer(9 - ringNummer.nummer))
        } else {
            Veld(lijn.cwLijn(), ringNummer)
        }

        KardinaleRichting.COUNTER -> if (lijn == Lijn.I) {
            // De notatie flipt hier, dus het ringnummer moet worden aangepast
            Veld(lijn.counterLijn(), RingNummer(9 - ringNummer.nummer))
        } else {
            Veld(lijn.counterLijn(), ringNummer)
        }
    }
}

fun String.veld(): Veld {
    require(this.length == 2)
    val lijnChar = this.first()
    val lijn = Lijn.entries.find { it.name == lijnChar.uppercase() }!!
    val ringChar = this.last()
    val ringNumber = ringChar.digitToInt()
    return Veld(lijn, RingNummer(ringNumber))
}
