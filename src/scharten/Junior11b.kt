package scharten

object Junior11b {
    const val DEPTH = 3

    @JvmStatic
    fun main(args: Array<String>) {
        println("Typ het nummer van de optie om een zet te doen")
        println("Typ iets wat geen nummer is (zoals 'b') om een zet terug te doen")
        val startpositie = "e1".veld()
        var huidigePositie = startpositie
        var gekozenWoorden = listOf(listOf(startpositie))
        val indexHistory = mutableListOf(1)
        while (true) {
            println()
            printVisueleWandelingRepresentatie(gekozenWoorden, huidigePositie)
            val opties = vindOptiesTotSpatie(huidigePositie, gekozenWoorden, indexHistory.last())
            println(gekozenWoorden.flatten().joinToString(separator = "") { veld -> veld2Word(veld) })
            opties.forEachIndexed { index, optie ->
                val woord = optie.flatten().joinToString(separator = "") { veld -> veld2Word(veld) }
                println("${index + 1}: $woord")
            }
            val line = readln()
            if (line.toIntOrNull() == null) {
                gekozenWoorden = gekozenWoorden.dropLast(1)
                huidigePositie = gekozenWoorden.last().last()
                if (indexHistory.size > 1) {
                    indexHistory.removeLast()
                }
                continue
            }
            val gekozenOptie = line.toInt() - 1
            gekozenWoorden = opties[gekozenOptie]
            huidigePositie = gekozenWoorden.last().last()
            indexHistory += gekozenWoorden.sumOf { it.size }
        }
    }

    fun vindOptiesTotSpatie(
        huidigePositie: Veld,
        gekozenWoorden: List<List<Veld>>,
        laatsteIndex: Int
    ): List<List<List<Veld>>> {
        val index = gekozenWoorden.sumOf { it.size }
        val currentDepth = index - laatsteIndex
        if (veld2Word(huidigePositie) == " " || veld2Word(huidigePositie) == "." || gekozenWoorden.size >= 80 || currentDepth >= DEPTH) {
            return listOf(gekozenWoorden)
        }
        val leegBordOpties = Paard.leegBordZetten(huidigePositie)
        val nogNietGekozenOpties = (leegBordOpties - gekozenWoorden.flatten().toSet()).toList()
        return nogNietGekozenOpties.flatMap {
            val nieuwWoord = gekozenWoorden.last() + it
            vindOptiesTotSpatie(
                it,
                gekozenWoorden.dropLast(1) + listOf(nieuwWoord),
                laatsteIndex
            )
        }
    }

    fun printVisueleWandelingRepresentatie(gekozenWoorden: List<List<Veld>>, huidigePositie: Veld) {
        val bord = mutableMapOf(
            Lijn.O to MutableList(8) { "  " },
            Lijn.A to MutableList(8) { "  " },
            Lijn.B to MutableList(8) { "  " },
            Lijn.C to MutableList(8) { "  " },
            Lijn.D to MutableList(8) { "  " },
            Lijn.E to MutableList(8) { "  " },
            Lijn.F to MutableList(8) { "  " },
            Lijn.G to MutableList(8) { "  " },
            Lijn.H to MutableList(8) { "  " },
            Lijn.I to MutableList(8) { "  " },
        )
        gekozenWoorden.forEach { gekozenWoord ->
            gekozenWoord.forEach { veld ->
                bord[veld.lijn]!![veld.ringNummer.arrayIdx()] = "OO"
            }
        }

        bord[huidigePositie.lijn]!![huidigePositie.ringNummer.arrayIdx()] = "XX"

        println("   o  a  b  c  d  e  f  g  h  i")
        (7 downTo 0).forEach { rij ->
            print(rij + 1)
            println(
                bord.entries.joinToString(
                    prefix = "|",
                    separator = " ",
                    postfix = "|"
                ) { (_, velden) -> velden[rij] })
        }
    }

    val wordMap = mapOf(
        Lijn.O to "ideiG s ".toCharArray(),
        Lijn.A to "d rets l".toCharArray(),
        Lijn.B to " ren.ead".toCharArray(),
        Lijn.C to "eiedpedv".toCharArray(),
        Lijn.D to ".ei e je".toCharArray(),
        Lijn.E to "Kseret n".toCharArray(),
        Lijn.F to "indrge s".toCharArray(),
        Lijn.G to " a ev n ".toCharArray(),
        Lijn.H to " inaieeg".toCharArray(),
        Lijn.I to "aaenrnrr".toCharArray(),
    )

    fun veld2Word(veld: Veld): String = wordMap[veld.lijn]!![veld.ringNummer.nummer - 1].toString()

}