package scharten

object Paard : StukType {
    override fun leegBordZetten(huidigVeld: Veld): Set<Veld> = listOf(
        listOf(KardinaleRichting.NOORD, KardinaleRichting.NOORD, KardinaleRichting.CW),
        listOf(KardinaleRichting.NOORD, KardinaleRichting.NOORD, KardinaleRichting.COUNTER),
        listOf(KardinaleRichting.NOORD, KardinaleRichting.CW, KardinaleRichting.CW),
        listOf(KardinaleRichting.NOORD, KardinaleRichting.COUNTER, KardinaleRichting.COUNTER),

        listOf(KardinaleRichting.ZUID, KardinaleRichting.ZUID, KardinaleRichting.CW),
        listOf(KardinaleRichting.ZUID, KardinaleRichting.ZUID, KardinaleRichting.COUNTER),
        listOf(KardinaleRichting.ZUID, KardinaleRichting.CW, KardinaleRichting.CW),
        listOf(KardinaleRichting.ZUID, KardinaleRichting.COUNTER, KardinaleRichting.COUNTER),
    ).mapNotNull { (move1, move2, move3) ->
        huidigVeld.aangrenzendVeld(move1)?.aangrenzendVeld(move2)?.aangrenzendVeld(move3)
    }.toSet()
}