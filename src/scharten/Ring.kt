package scharten

enum class Ring(val attackValue: Int) {
    DOUBLE(2), FAT(1), TREBLE(3), LITTLE(1)
}