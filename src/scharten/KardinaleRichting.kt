package scharten

enum class KardinaleRichting {
    // Verticale zetten
    NOORD,
    ZUID,

    // Horizontale zetten
    CW,
    COUNTER,
}