package scharten

object Junior11a {
    @JvmStatic
    fun main(args: Array<String>) {
        println("Typ het nummer van de optie om een zet te doen")
        println("Typ iets wat geen nummer is (zoals 'b') om een zet terug te doen")
        val startpositie = "h8".veld()
        var huidigePositie = startpositie
        val gekozenOpties = mutableListOf(startpositie)
        while (true) {
            println()
            val leegBordOpties = Paard.leegBordZetten(huidigePositie)
            val nogNietGekozenOpties = (leegBordOpties - gekozenOpties.toSet()).toList()
            printVisueleWandelingRepresentatie(gekozenOpties, huidigePositie, nogNietGekozenOpties)
            gekozenOpties.chunked(10).map { velden ->
                println(velden.joinToString(separator = " ") { veld -> veld2Word(veld) })
            }
            nogNietGekozenOpties.forEachIndexed { index, veld -> println("${index + 1}: $veld ${veld2Word(veld)}") }
            val line = readln()
            if (line.length != 1 || !line.single().isDigit()) {
                gekozenOpties.removeLast()
                huidigePositie = gekozenOpties.last()
                continue
            }
            val gekozenOptie = line.single().digitToInt() - 1
            huidigePositie = nogNietGekozenOpties[gekozenOptie]
            gekozenOpties += huidigePositie
        }
    }

    fun printVisueleWandelingRepresentatie(gekozenOpties: List<Veld>, huidigePositie: Veld, opties: List<Veld>) {
        val bord = mutableMapOf(
            Lijn.O to MutableList(8) { "  " },
            Lijn.A to MutableList(8) { "  " },
            Lijn.B to MutableList(8) { "  " },
            Lijn.C to MutableList(8) { "  " },
            Lijn.D to MutableList(8) { "  " },
            Lijn.E to MutableList(8) { "  " },
            Lijn.F to MutableList(8) { "  " },
            Lijn.G to MutableList(8) { "  " },
            Lijn.H to MutableList(8) { "  " },
            Lijn.I to MutableList(8) { "  " },
        )
        gekozenOpties.forEachIndexed { index, veld ->
            bord[veld.lijn]!![veld.ringNummer.arrayIdx()] = padNaarTweeChars(index)
        }

        opties.forEach { veld ->
            bord[veld.lijn]!![veld.ringNummer.arrayIdx()] = "??"
        }

        bord[huidigePositie.lijn]!![huidigePositie.ringNummer.arrayIdx()] = "XX"

        println("   o  a  b  c  d  e  f  g  h  i")
        (7 downTo 0).forEach { rij ->
            print(rij + 1)
            println(
                bord.entries.joinToString(
                    prefix = "|",
                    separator = " ",
                    postfix = "|"
                ) { (_, velden) -> velden[rij] })
        }
    }

    fun padNaarTweeChars(int: Int) = if (int in 0..9) "0$int" else "$int"

    val wordMap = mapOf(
        Lijn.O to listOf("je", "voet", "tij", "stop", "niet", "e", "van", "der"),
        Lijn.A to listOf("uit", "noeg", "rten", "ken", "te", "uit", "veelt", "van"),
        Lijn.B to listOf("in", "zal", "ste", "ge", "ver", "tan", "en", "om"),
        Lijn.C to listOf("ook", "die", "scha", "scha", "zelf", "har", "raad", "die"),
        Lijn.D to listOf("ze", "kun", "moe", "kend", "te", "neemt", "eens", "daagt"),
        Lijn.E to listOf("der", "kan", "met", "nen", "te", "zich", "ter", "de"),
        Lijn.F to listOf("darts", "rten", "tal", "kroeg", "speelt", "de", "ken", "het"),
        Lijn.G to listOf("ent", "maar", "het", "scha", "ma", "lief", "maar", "win"),
        Lijn.H to listOf("haar", "speelt", "de", "ne", "dens", "bal", "dat", "een"),
        Lijn.I to listOf("dat", "is", "va", "best", "ste", "meis", "ter", "zich"),
    )

    fun veld2Word(veld: Veld): String = wordMap[veld.lijn]!![veld.ringNummer.nummer - 1]

}