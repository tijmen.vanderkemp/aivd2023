package scharten

/**
 * 1-indexed
 */
@JvmInline
value class RingNummer(val nummer: Int) {
    init {
        require(nummer in 1..8)
    }

    fun ring() = when (nummer) {
        1, 8 -> Ring.DOUBLE
        2, 7 -> Ring.FAT
        3, 6 -> Ring.TREBLE
        4, 5 -> Ring.LITTLE
        else -> throw IllegalArgumentException()
    }

    operator fun plus(other: Int) = RingNummer(nummer + other)
    operator fun minus(other: Int) = RingNummer(nummer - other)

    fun arrayIdx() = nummer - 1
}