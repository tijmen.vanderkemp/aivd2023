package scharten

sealed interface StukType {
    fun leegBordZetten(huidigVeld: Veld): Set<Veld>
}